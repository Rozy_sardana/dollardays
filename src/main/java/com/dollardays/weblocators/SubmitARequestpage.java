package com.dollardays.weblocators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.dollardays.common.BasePage;

public class SubmitARequestpage extends BasePage {

	public static final String SubmitARequest_hlink = "Submit A Request";
	public static final String Email_id = "ctl00_cphContent_txtemail";
	public static final String phoneno_id = "ctl00_cphContent_txtPhone";
	public static final String SelectRequestType_id = "ctl00_cphContent_drpReason";
	public static final String orderno_id = "ctl00_cphContent_txtOrderNo";
	public static final String messagebox_id = "ctl00_cphContent_txtNote";
	public static final String captcha_class = "recaptcha-checkbox-border";
	public static final String Submit_id = "ctl00_cphContent_btnCreate";
	private static WebElement element = null;

	public SubmitARequestpage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	// hyperlink
	public WebElement hlink_SubmitARequest(WebDriver driver) {
		element = driver.findElement(By.xpath("//*[@id=\"loginDropdownMenu\"]/li[9]/a"));
		return element;
	}

	// Emailid
	public WebElement txt_Email(WebDriver driver) {
		element = driver.findElement(By.id("ctl00_cphContent_txtemail"));
		return element;

	}

	public WebElement txt_phoneno(WebDriver driver) {
		return element = driver.findElement(By.id("ctl00_cphContent_txtPhone"));

	}

	public WebElement dropdwn_SelectRequestType(WebDriver driver) {
		return element = driver.findElement(By.id("ctl00_cphContent_drpReason"));

	}

	public WebElement txt_orderno(WebDriver driver) {
		return element = driver.findElement(By.id("ctl00_cphContent_txtOrderNo"));

	}

	public WebElement txt_messagebox(WebDriver driver) {
		return element = driver.findElement(By.id("ctl00_cphContent_txtNote"));

	}

	public WebElement btn_Submit(WebDriver driver) {
		return element = driver.findElement(By.id("ctl00_cphContent_btnCreate"));

	}

}
