package com.dollardays.tests;

import java.io.IOException;
import java.lang.reflect.Method;

import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.dollardays.common.BaseTest;
import com.dollardays.common.CommonUtilities;
import com.dollardays.common.ReadExcel;
import com.dollardays.weblocators.SignInPage;
import com.dollardays.weblocators.SignOutPage;
import com.dollardays.weblocators.SubmitARequestpage;

public class SubmitARequestpageScript extends BaseTest {

	private Logger logger = LoggerFactory.getLogger(SubmitARequestpageScript.class.getName());

	private SignInPage signInPage = null;
	private static final String screenshots = null;

	private SubmitARequestpage submitARequestpage = null;
	private SignOutPage signOutPage = null;
	private ExtentTest logTest;

	@BeforeSuite
	public void beforeSuite() throws IOException {
		initialization();
		signInPage = new SignInPage(driver);
		submitARequestpage = new SubmitARequestpage(driver);
		signOutPage = new SignOutPage(driver);
		CommonUtilities.captureScreenShot(driver, "browser initilized", screenshots);
		logger.info("browser initialized");
	}

	@Test(priority = 1)//signing into the account 
	public void Tc_01_SignInPage() throws InterruptedException, IOException {
//		logger.debug("Tc_01_SignInPage - start");
//		signInPage.clickSignInButton();
//		signInPage.clickSignInnerLink();
//		signInPage.sendUserNamePassword();
//		signInPage.clickSigninActionButton();
//		logger.debug("Tc_01_SignInPage - End");
		signInMethod();
		CommonUtilities.captureScreenShot(driver, "usersignedin", screenshots);
		logger.info("user signedin");
	}

	@Test(priority = 2) // clicking SubmitARequest hyperlink from signin img

	public void submitRequestFromMenu() throws InterruptedException, IOException {
		signInPage.clickSignInButton();
		submitARequestpage.hlink_SubmitARequest(driver).click();
		CommonUtilities.captureScreenShot(driver, "SubmitARequest hypelink is clicked", screenshots);
		logger.info("SubmitARequest hyperlink is clicked");

	}

	@Test(priority = 3)// Entering all the details in submitarequest page 
	public void submitNewRequest() throws IOException {
		submitARequestpage.txt_Email(driver).clear(); // clearing the existing data in the textbox
		submitARequestpage.txt_Email(driver).sendKeys(CommonUtilities.getPropertyValue("Email")); //entering Email address
		submitARequestpage.txt_phoneno(driver).sendKeys(CommonUtilities.getPropertyValue("phoneno"));//Entering phone no 
		Select requestTypeSelect = new Select(submitARequestpage.dropdwn_SelectRequestType(driver));//creating a object for select class to select a value from dropbox
		requestTypeSelect.selectByVisibleText(CommonUtilities.getPropertyValue("requesttype"));//selecting a value from dropbox
		submitARequestpage.txt_orderno(driver).sendKeys(CommonUtilities.getPropertyValue("orderno"));//entering the orderno
		submitARequestpage.txt_messagebox(driver).sendKeys(CommonUtilities.getPropertyValue("message"));//entering the message in description box
		submitARequestpage.btn_Submit(driver).click();//clicking submit btn
		CommonUtilities.captureScreenShot(driver, "lookingforcaptcha", screenshots);
		logger.info("user entered all the details");

	}

	@Test(priority = 4) // clicking on signout btn to signout from account 
	public void logout() throws IOException {

		signInPage.clickSignInButton();
		// SubmitARequestpageScript log1 =new SubmitARequestpageScript();
		signOutPage.btn_logout(driver).click();

//	  
		logger.info("user signedout");
		CommonUtilities.captureScreenShot(driver, "signout", screenshots);

	}

	@BeforeMethod
	public void beforeMethod(Method method) throws IOException {
		
		logTest = getExtentReport().createTest(method.getName());
		ReadExcel.cleanUpFile(CommonUtilities.getPropertyValue("testreportfilepath"));
//	  driver.findElement(By.className("rfk_wbh overlap")).click();
	}

	@AfterMethod
	public void afterMethod(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			logTest.fail("Test Case Failed is " + result.getThrowable());
		} else if (result.getStatus() == ITestResult.SKIP) {
			logTest.skip("Test Case Skipped is " + result.getThrowable());
		} else
			logTest.pass("pass");

		getExtentReport().flush();

	}

	@BeforeClass
	public void beforeClass() {
	}

	@AfterClass
	public void afterClass() {
	}

	@BeforeTest
	public void beforeTest() {

	}

	@AfterTest
	public void afterTest() {
	}

	@AfterSuite
	public void afterSuite() {

	}

}
