package com.dollardays.tests;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.dollardays.common.BaseTest;
import com.dollardays.common.CommonUtilities;
import com.dollardays.common.ReadExcel;
import com.dollardays.weblocators.MyProfilePage;

public class EmailPreferencesValidations extends BaseTest{
	
	private static MyProfilePage MyProfilePage=null;
	 String ExpectedValue=null,ActualValue=null,Actual_Title=null,Expected_Title=null;
	  
	 private Logger logger = LoggerFactory.getLogger(EmailPreferencesValidations.class);
	 ExtentTest extentTest;


	 
	  private JavascriptExecutor js = null;
	 // @Test(priority=0)
	  public void emailPreferences_UserEmailAddressValidation() throws IOException, InterruptedException {
		
		  ExtentTest EmailPreferencesValidations = this.getExtentReport().createTest("emailPreferences_UserEmailAddressValidation");  
		 
		  String ActualUserName=MyProfilePage.Email_Username.getText();
		  js.executeScript("window.scrollBy(0,1000)");
		  
		// System.out.println(ActualUserName);
		 String ExpectedUserName=ReadExcel.getData(CommonUtilities.getPropertyValue("testdatapath"), "Test", 1, 0); //CommonUtilities.getPropertyValue("username");
		// System.out.println(ExpectedUserName);
		  try {
			  Assert.assertEquals(ActualUserName, ExpectedUserName);
			 // System.out.println("Assertion Pass");
			  
			  logger.info("Test1 : The displayed email address is matching with the actual UserName/Emailaddress..As Expected");
			  ReadExcel.writeresult(".\\Results\\AccountPageResults.xlsx", 3, 0, 1,"Test1 : The displayed email address is matching with the actual UserName/Emailaddress..As Expected");
			 
			  
			  CommonUtilities.captureScreenShot(BaseTest.driver,"Test1_UserEmailAddressValidation", "folder"); //Folder parameter is not getting used
			 
			  EmailPreferencesValidations.pass("Test1 : The displayed email address is matching with the actual UserName/Emailaddress..As Expected");
			 }
			  catch(AssertionError e)
			  {
				 // System.out.println("Assertion Fail     :"+ e);
				  ReadExcel.writeresult(".\\Results\\AccountPageResults.xlsx", 3, 0, 1,"Test 1 :The displayed email address is not matching with the actual Username/Emailaddress..Not as Expected");
				  logger.debug("Test 1 :The displayed email address is not matching with the actual Username/Emailaddress..Not as Expected");
				  logger.debug("Error Message  :"+e.getMessage());
				  EmailPreferencesValidations.fail("Test 1 :The displayed email address is not matching with the actual Username/Emailaddress..Not as Expected");
			     // System.out.println("The displayed email address is not matching with the actual Username/Emailaddress..Not as Expected");
			  }
		  logger.info("Test#1 Finish"); 
	  }
	 
	 
    // @Test(priority=1)
       public void emailPreferences_Checkbox_Validation_AtPageLaunch() throws IOException, InterruptedException {
    	  //JavascriptExecutor js = (JavascriptExecutor)BaseTest.driver;
	   // MyProfilePage= new MyProfilePage(BaseTest.driver);
    	 ExtentTest EmailPreferencesValidations = this.getExtentReport().createTest("emailPreferences_Checkbox_Validation_AtPageLaunch");
    	
	  // js.executeScript("window.scrollBy(0,1000)");
	  // logger.info("Test 2: WIndow scrolled by 1000 pixels vertically");
	  
	   CommonUtilities.captureScreenShot(BaseTest.driver,"Test2_emailPreferences_CheckBox_Verification_AtPageLaunch", "folder");
	   boolean A1=true;
	   try {
	   Assert.assertEquals(MyProfilePage.NewsLetter_CheckBox.isSelected(),A1);
       logger.info("Test 2 :NewsLetter Subscription Check Box is Prechecked at the time of First page Launch as Expected");
       ReadExcel.writeresult(".\\Results\\AccountPageResults.xlsx", 4, 0, 1,"Test 2 :NewsLetter Subscription Check Box is Prechecked at the time of First page Launch as Expected");
       EmailPreferencesValidations.pass("Test 2 :NewsLetter Subscription Check Box is Prechecked at the time of First page Launch as Expected");
	   }
	     catch(AssertionError e) {
	    	//ReadExcel.writeresult(".\\Results\\AccountPageResults.xlsx", 4, 0, 1,"Test 2 :NewsLetter Subscription Check Box is Not Prechecked at the time of First page Launch.Not as Expected",false);
	  	    logger.debug("Test 2 :NewsLetter Subscription Check Box is Not Prechecked at the time of First page Launch.Not as Expected");
		    logger.debug("Error Message  :"+e.getMessage());  
		    EmailPreferencesValidations.fail("Test 2 :NewsLetter Subscription Check Box is Not Prechecked at the time of First page Launch.Not as Expected");
	     }
	   logger.info("Test#2 Finish");
     }
  
     
     @Test(priority=2)
       public void emailPreferences_checkbox_validation_AtClick() throws InterruptedException, IOException {
    	// JavascriptExecutor js = (JavascriptExecutor)BaseTest.driver;
    	// MyProfilePage= new MyProfilePage(BaseTest.driver);
    	 ExtentTest EmailPreferencesValidations = this.getExtentReport().createTest("emailPreferences_checkbox_validation_AtClick");
  	  js.executeScript("window.scrollBy(0,1000)");
  	   logger.info("Test 3: WIndow scrolled by 1000 pixels vertically");
	     if(MyProfilePage.NewsLetter_CheckBox.isSelected())
	      {  
	  
	        MyProfilePage.click_NewsLetter_CheckBox();
	        logger.info("Test 3 :Unchecked the checkbox");
	     	MyProfilePage.click_EmailPreferences_SaveChanges();
	     	logger.info("Test 3 :Clicked save changes button");
	     	MyProfilePage.click_User_Dropdown_Toggle();
	     	logger.info("Test 3 :Clicked User toggle");
	     	Thread.sleep(1000);
	    	MyProfilePage.click_User_Dropdown_Toggle_Signout();
	    	logger.info("Test 3 :Clicked singout");
	    	Thread.sleep(1000);
	    	 signInMethod();
	    	 logger.info("Test 3 :Called Signin method and logged back in");
	  		
		   MyProfilePage.click_User_Dropdown_Toggle();
		   logger.info("Test 3: User Toggle clicked");
		    MyProfilePage.click_User_Dropdown_Toggle_Accounts();
		    logger.info("Test 3: Accounts link clicked");
		   Thread.sleep(3000);
		   MyProfilePage.click_profileLink();
		   logger.info("Test 3: My Profile link clicked");
		  js.executeScript("window.scrollBy(0,1000)");
		   logger.info("Test 3: WIndow scrolled by 1000 pixels vertically");
		   CommonUtilities.captureScreenShot(BaseTest.driver,"Test3_emailPreferences_CheckBox_Verification_AtClick", "folder");
		   try
		     {
		     Assert.assertEquals(MyProfilePage.NewsLetter_CheckBox.isSelected(),false);
		     logger.info("Test 3 :Once unchecked the Newsletter subscription checkbox remains unchanged until further changes to it:As Expected");
		     ReadExcel.writeresult(".\\Results\\AccountPageResults.xlsx",5, 0, 1,"Test 3 :Once unchecked the Newsletter subscription checkbox remains unchanged until further changes to it:As Expected");
		     EmailPreferencesValidations.pass("Test 3 :Once unchecked the Newsletter subscription checkbox remains unchanged until further changes to it:As Expected");
		    }
		    catch(AssertionError e)
		      {
		      ReadExcel.writeresult(".\\Results\\AccountPageResults.xlsx", 5, 0, 1,"Test 3 :The changes to the Newsletter checkbox is not remaining unchanged:Not as expected");
              logger.debug("Test 3 :The changes to the Newsletter checkbox is not remaining unchanged:Not as expected");
			  logger.debug("Error Message  :"+e.getMessage());
			  EmailPreferencesValidations.fail("Test 3 :The changes to the Newsletter checkbox is not remaining unchanged:Not as expected");
			  }
		   }
	  
	     MyProfilePage.click_NewsLetter_CheckBox();
	     MyProfilePage.click_EmailPreferences_SaveChanges();
     	logger.info("Test#3 Finish");
        }
     
     
       // @Test(priority=3)
        public void  EmailPreferences_LinksValidation() throws IOException, InterruptedException {
        	//JavascriptExecutor js = (JavascriptExecutor)BaseTest.driver;
	       //MyProfilePage= new MyProfilePage(BaseTest.driver);
        	ExtentTest EmailPreferencesValidations = this.getExtentReport().createTest("EmailPreferences_LinksValidation");
       	js.executeScript("window.scrollBy(0,1000)");
       	logger.info("Test 4: WIndow scrolled by 1000 pixels vertically");
	      MyProfilePage.click_MyAccountTermsandConditions();
	      logger.info("Test 4: Terms and Conditions link clicked");
	     // String Expected_Title = "Policies - DollarDays";
	      Expected_Title = CommonUtilities.getPropertyValue("TermsAndConditions_ExpectedPageTitle");
	      String Actual_Title = BaseTest.driver.getTitle();
	      CommonUtilities.captureScreenShot(BaseTest.driver,"Test4_emailPreferences_TermsandConditionsLink_Validation_AtClick", "folder");
	      try {
	       Assert.assertEquals(Actual_Title, Expected_Title);
	      // System.out.println("Assertion Pass");
	       logger.info("Test 4 :Terms and Conditions link is directing to 'Policies-DollarDays' page..As Expected");
	       ReadExcel.writeresult(".\\Results\\AccountPageResults.xlsx", 6, 0, 1,"Test 4 :Terms and Conditions link is directing to 'Policies-DollarDays' page..As Expected");
	       EmailPreferencesValidations.pass("Test 4 :Terms and Conditions link is directing to 'Policies-DollarDays' page..As Expected");  
	      }
	          catch(AssertionError e)
	            {
	        	  //System.out.println("Assertion Fail     :"+ e);
	        	  ReadExcel.writeresult(".\\Results\\AccountPageResults.xlsx", 6, 0, 1,"Test 4:Terms and Conditions link is Not directing to 'Policies-DollarDays'page..Not as Expected");
	              logger.debug("Test 4:Terms and Conditions link is Not directing to 'Policies-DollarDays' page..Not as Expected");
	              logger.debug("Error Message  : "+e.getMessage()); 
	              EmailPreferencesValidations.fail("Test 4:Terms and Conditions link is Not directing to 'Policies-DollarDays' page..Not as Expected");
	            }
	   
	      BaseTest.driver.navigate().back();
	      logger.info("Test 4 :Navigated back to My profile page");
	  	js.executeScript("window.scrollBy(0,1000)");
       	logger.info("Test 4: WIndow scrolled by 1000 pixels vertically");
	      MyProfilePage.click_PrivacyandSecurityStatement();
	      logger.info("Test 4: PrivacyandSecurityStatement link clicked");
	      Actual_Title = BaseTest.driver.getTitle();
	      Expected_Title=CommonUtilities.getPropertyValue("PrivacyandSecurityStatement_ExpectedPageTitle");
	      CommonUtilities.captureScreenShot(BaseTest.driver,"Test4_emailPreferences_PrivacyandSecurityStatementLink_Validation_AtClick", "folder");
	      try
	         {
	          Assert.assertEquals(Actual_Title, Expected_Title);
	         // System.out.println("Assertion Pass");
	          logger.info("Test 4:Privacy & Security Statement link is directing you to 'Policies-DollarDays'Page..As Expected");
	          ReadExcel.writeresult(".\\Results\\AccountPageResults.xlsx", 7, 0, 1,"Test 4:Privacy & Security Statement link is directing you to 'Policies-DollarDays' Page..As Expected");
	          EmailPreferencesValidations.pass("Test 4:Privacy & Security Statement link is directing you to 'Policies-DollarDays'Page..As Expected");
	         }
	          catch(AssertionError e)
	      		{
	        	// System.out.println("Assertion Fail     :"+ e);
	        	   ReadExcel.writeresult(".\\Results\\AccountPageResults.xlsx", 7, 0, 1,"Test 4:Privacy & Security Statement link is Not directing to 'Policies-DollarDays' Page..Not as Expected");
	        	  logger.debug("Test 4:Privacy & Security Statement link is Not directing to 'Policies-DollarDays'Page..Not as Expected");
	        	  logger.debug("Error Message  : "+e.getMessage());
	        	  EmailPreferencesValidations.fail("Test 4:Privacy & Security Statement link is Not directing to 'Policies-DollarDays'Page..Not as Expected");
	      		}
	  
	      logger.info("Test#4 Finish");
        }
  
      // @Test(priority=4)
        public void termAndConditionsPage_LinksValidation() throws InterruptedException, IOException {
        	//MyProfilePage = new MyProfilePage(BaseTest.driver);
        	 
        	//Validating "service@dollardays.com" link
    	   ExtentTest EmailPreferencesValidations = this.getExtentReport().createTest("termAndConditionsPage_LinksValidation");
    	   
        	logger.info("Test 5:Validating Services@dollardays.com");
        	List<WebElement> linkElements = MyProfilePage.ServicesADDDotCom_links;
        	int n=8;
        	for(int i=0;i<linkElements.size();i++,n++)
        	{  //ExpectedValue="mailto:service@dollardays.com";
        		ExpectedValue=CommonUtilities.getPropertyValue("service@dollardays.com_Link_ExpectedValue");
        	ActualValue= linkElements.get(i).getAttribute("href");
        	try {
        		Assert.assertEquals(ActualValue, ExpectedValue);
        		//System.out.println("Assertion Pass");
        		logger.info("Test 5:"+i+"Services@dollardays.com link is directing to outlook..As Expected");
        		 ReadExcel.writeresult(".\\Results\\AccountPageResults.xlsx", n, 0, 1,"Test 5: Link Number"+i+"Services@dollardays.com link is directing to outlook..As Expected'..As Expected");
        		 EmailPreferencesValidations.pass("Test 5:"+i+"Services@dollardays.com link is directing to outlook..As Expected");
        	}
        	catch(AssertionError e)
        	{
        		//System.out.println("Assertion Fail     :"+ e);
        		 ReadExcel.writeresult(".\\Results\\AccountPageResults.xlsx", n, 0, 1,"Test 5: Link Number "+i+" Services@dollardays.com link is not directing to outlook..Not as Expected");
        		logger.debug("Test 5: "+i+" Services@dollardays.com link is not directing to outlook..Not as Expected");
        		logger.debug("Error Message  : "+e.getMessage());
        		EmailPreferencesValidations.fail("Test 5: "+i+" Services@dollardays.com link is not directing to outlook..Not as Expected");
        	}
        	}
	  
        	//Validating  "Please review our full shipping terms here." link
        	logger.info("Test 5:Validating  \"Please review our full shipping terms here.\" link");
        	MyProfilePage.PleaseReviewFullShippingTerms_link.click();
        	logger.info("Test 5:Clicked \"Please review our full shipping terms here.\" link");
        	Actual_Title=BaseTest.driver.getTitle();
        	//Expected_Title="Shipping Policy - DollarDays";
        	Expected_Title=CommonUtilities.getPropertyValue("PleaseReviewOurFullShippingTermsHere._Link_ExpectedValue");
        	try {
        		Assert.assertEquals(Actual_Title, Expected_Title);
        		//System.out.println("Assertion Pass");
        		logger.info("Test 5 :'Please review our full shipping terms here.'link is directing to 'Shipping Policy - DollarDays' page..As Expected");
        		 ReadExcel.writeresult(".\\Results\\AccountPageResults.xlsx",11 , 0, 1,"Test 5 :'Please review our full shipping terms here.'link is directing to 'Shipping Policy - DollarDays' page..As Expected");
        		 EmailPreferencesValidations.pass("Test 5 :'Please review our full shipping terms here.'link is directing to 'Shipping Policy - DollarDays' page..As Expected");
        	}
        	catch(AssertionError e)
        	{
			 // System.out.println("Assertion Fail     :"+ e);
        		ReadExcel.writeresult(".\\Results\\AccountPageResults.xlsx",11 , 0, 1,"Test 5:'Please review our full shipping terms here.'link is not directing to 'Shipping Policy - DollarDays' Page..Not as Expected");
		      logger.debug("Test 5:'Please review our full shipping terms here.'link is not directing to 'Shipping Policy - DollarDays' Page..Not as Expected");
		      logger.debug("Error Message  : "+e.getMessage());
		      EmailPreferencesValidations.fail("Test 5:'Please review our full shipping terms here.'link is not directing to 'Shipping Policy - DollarDays' Page..Not as Expected");
        	}
        	BaseTest.driver.navigate().back();
        	logger.info("Test 5:Navigated back to Terms and Conditions page");
	 
        	//Validating "support@dollardays.com" link
        	logger.info("Test 5:Validating  \"support@dollardays.com\" link");
        	WebElement Element = MyProfilePage.SupportADDDotCom_link;
        	ActualValue=Element.getAttribute("href");
        	//ExpectedValue="mailto:support@dollardays.com";
        	ExpectedValue=CommonUtilities.getPropertyValue("support@dollardays.com_link_ExpectedValue");
        	try {
        		Assert.assertEquals(ActualValue, ExpectedValue);
        		//System.out.println("Assertion Pass");
        		logger.info("Test 5:\"support@dollardays.com\" link is directing to outlook..As Expected");
        		ReadExcel.writeresult(".\\Results\\AccountPageResults.xlsx",12, 0, 1,"Test 5:\"support@dollardays.com\" link is directing to outlook..As Expected");
        		EmailPreferencesValidations.pass("Test 5:\"support@dollardays.com\" link is directing to outlook..As Expected");
        	}
        	catch(AssertionError e)
        	{
        		//System.out.println("Assertion Fail     :"+ e);
        		ReadExcel.writeresult(".\\Results\\AccountPageResults.xlsx",12, 0, 1,"Test 5:\"support@dollardays.com\" link is not directing to outlook..Not as Expected");
        		logger.debug("Test 5:\"support@dollardays.com\" link is not directing to outlook..Not as Expected");
        		logger.debug("Error Message  : "+e.getMessage());
        		EmailPreferencesValidations.fail("Test 5:\"support@dollardays.com\" link is not directing to outlook..Not as Expected");       	
        	}
	 
	 
        	//Validating "www.adr.org." link
        	logger.info("Test 5:Validating  \"www.adr.org.\" link");
        	//System.out.println("Parent Title    :  " + BaseTest.driver.getTitle());
	 
        	MyProfilePage.click_WWWDotADRDotORG_link();
        	logger.info("Test 5:Clicked \"www.adr.org.\" link");
        	Set<String> allwindows=BaseTest.driver.getWindowHandles();
        	ArrayList<String> tabs=new ArrayList<>(allwindows);
        	
        	BaseTest.driver.switchTo().window(tabs.get(1));
        	logger.info("Switched the focus to Child Tab");
        	//System.out.println("New Tab Title   :   " + BaseTest.driver.getTitle());
        	Actual_Title=BaseTest.driver.getTitle();
        	//Expected_Title="American Arbitration Association | ADR.org";
        	Expected_Title=CommonUtilities.getPropertyValue("www.adr.org._link_ExpectedTitle");
        	try {
        		Assert.assertEquals(Actual_Title, Expected_Title);
        		//System.out.println("Assertion Pass");
        		logger.info("Test 5:'www.adr.org.'link is directing to 'American Arbitration Association | ADR.org' tab..As Expected");
        		ReadExcel.writeresult(".\\Results\\AccountPageResults.xlsx",13, 0, 1,"Test 5:'www.adr.org.'link is directing to 'American Arbitration Association | ADR.org' tab..As Expected");
        		EmailPreferencesValidations.pass("Test 5:'www.adr.org.'link is directing to 'American Arbitration Association | ADR.org' tab..As Expected");
        	}
        	catch(AssertionError e)
        	{
        		//System.out.println("Assertion Fail     :"+ e);
        		ReadExcel.writeresult(".\\Results\\AccountPageResults.xlsx",13, 0, 1,"Test 5:'www.adr.org.'link is not directing to 'American Arbitration Association | ADR.org' tab..Not as Expected ");
        		logger.debug("Test 5:'www.adr.org.'link is not directing to 'American Arbitration Association | ADR.org' tab..Not as Expected");
        		logger.debug("Error Message  : "+e.getMessage());
        		EmailPreferencesValidations.fail("Test 5:'www.adr.org.'link is not directing to 'American Arbitration Association | ADR.org' tab..Not as Expected");
        	}
        	
        	BaseTest.driver.close();
        	logger.info("Closed the child Tab");
        	BaseTest.driver.switchTo().window(tabs.get(0));
        	logger.info("Changed the focus to parent tab");
        	//System.out.println("Parent Title    :  " + BaseTest.driver.getTitle());
 
   
        	//Validating "contact@dollardays.com." link
        	logger.info("Validating \"contact@dollardays.com.\" link");
        	Element = MyProfilePage.ContactADDDotCom_link;
        	ActualValue=Element.getAttribute("href");
        	//ExpectedValue="mailto:contact@dollardays.com";
        	ExpectedValue=CommonUtilities.getPropertyValue("contact@dollardays.com._link_ExpectedValue");
        		try {
        			Assert.assertEquals(ActualValue, ExpectedValue);
        			//System.out.println("Assertion Pass");
        			logger.info("Test 5:\"contact@dollardays.com\" link is directing to outlook..As Expected");
        			ReadExcel.writeresult(".\\Results\\AccountPageResults.xlsx",14, 0, 1,"Test 5:\"contact@dollardays.com\" link is directing to outlook..As Expected");
        			EmailPreferencesValidations.pass("Test 5:\"contact@dollardays.com\" link is directing to outlook..As Expected");
        		}
        		catch(AssertionError e)
        		{
        			//System.out.println("Assertion Fail     :"+ e);
        			ReadExcel.writeresult(".\\Results\\AccountPageResults.xlsx",14, 0, 1,"Test 5:\"contact@dollardays.com\" link is not directing to outlook..Not As Expected");
        			logger.debug("Test 5:\"contact@dollardays.com\" link is not directing to outlook..Not As Expected");
        			logger.debug("Error Message  : "+e.getMessage());
        			EmailPreferencesValidations.fail("Test 5:\"contact@dollardays.com\" link is not directing to outlook..Not As Expected");
        		}
 
        		logger.info("Test#5 Finish");
 }
  
  @BeforeMethod
  public void beforeMethod() {
	
  }

  @AfterMethod
  public void afterMethod() {
  }

  @BeforeClass
  public void beforeClass() {
  }

  @AfterClass
  public void afterClass() {
  }

  @BeforeTest
  public void beforeTest() throws InterruptedException, IOException {
	  initialization();
	  logger.info("Driver instantiated");
	  signInMethod();
	  logger.info("singned in to DollarDays");
	  
	  //The Below code will navigate you to My Profile page
	  MyProfilePage = new MyProfilePage(BaseTest.driver);
	  MyProfilePage.click_User_Dropdown_Toggle();
	  logger.info("Test 1: Main User Toggle clicked");
	  Thread.sleep(2000);
	  MyProfilePage.click_User_Dropdown_Toggle_Accounts();
	  logger.info("Test 1: Accounts link clicked");
	  Thread.sleep(2000);
	  MyProfilePage.click_profileLink();
	  logger.info("Test 1: My Profile link clicked");
	  js = (JavascriptExecutor)BaseTest.driver;
	  extentReportSetUp();
  }

  @AfterTest
  public void afterTest() {
		MyProfilePage.click_User_Dropdown_Toggle();
	  	MyProfilePage.click_User_Dropdown_Toggle_Signout();
	  	logger.info("Signed out from Dollardays");
	  	this.getExtentReport().flush();
	  	termination();
	  	
  }

}





























