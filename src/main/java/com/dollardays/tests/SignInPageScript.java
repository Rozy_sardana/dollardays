package com.dollardays.tests;

import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.dollardays.common.BaseTest;
import com.dollardays.weblocators.SignInPage;

public class SignInPageScript extends BaseTest {
	private SignInPage signInPage = null;
	private Logger logger = LoggerFactory.getLogger(SignInPageScript.class);

	@BeforeMethod
	public void setup() throws IOException {
		logger.debug("setup - start");
		
		initialization();
		signInPage = new SignInPage(driver);
		logger.debug("setup - end");
	}

	 @Test
	public void Tc_01_SignInPage() throws InterruptedException, IOException {
		logger.debug("Tc_01_SignInPage - start");
		ExtentTest signInTestCase = this.getExtentReport().createTest("Tc_01_SignInPage");
		this.signInPage.clickSignInButton();
		this.signInPage.clickSignInnerLink();
		this.signInPage.sendUserNamePassword();
		this.signInPage.clickSigninActionButton();
		logger.debug("Tc_01_SignInPage - end");
		boolean isTestPassed = this.signInPage.verifyWelcomeUsername();
		if (isTestPassed) {
			signInTestCase.pass("Sign In Test casse passed");
		} else {
			signInTestCase.fail("Sign In Test casse fail");
		}

		assertTrue(isTestPassed);

	}

	@Test
	public void Tc_02_SignInPageValidUsernameEmptyPassword() throws InterruptedException, IOException {
		logger.debug("Tc_02_SignInPageValidUsernameEmptyPassword - start");
		ExtentTest SignInPageValidEmailEmptyPasswordTestCase = this.getExtentReport()
				.createTest("Tc_02_SignInPageValidUsernameEmptyPassword");

		this.signInPage.clickSignInButton();
		this.signInPage.clickSignInnerLink();
		this.signInPage.sendValidUserNameEmptyPassword();
		this.signInPage.clickSigninActionButton();
		logger.debug("Tc_02_SignInPageValidUsernameEmptyPassword - end");
		boolean isTestPassed = this.signInPage.verifyValidUsernameEmptyPassword();
		if (isTestPassed) {
			SignInPageValidEmailEmptyPasswordTestCase.pass("SignInPageValidUsernameEmptyPassword passed");
		} else {
			SignInPageValidEmailEmptyPasswordTestCase.fail("SignInPageValidUsernameEmptyPassword fail");
		}

		assertTrue(isTestPassed);
	}

	 @Test
	public void Tc_03_SignInPageEmptyUsernameEmptyPassword() throws InterruptedException, IOException {
		logger.debug("Tc_03_SignInPageEmptyUsernameEmptyPassword - start");
		ExtentTest SignInPageEmptyUsernameEmptyPassword = this.getExtentReport()
				.createTest("Tc_03_SignInPageValidUsernameEmptyPassword");

		this.signInPage.clickSignInButton();
		this.signInPage.clickSignInnerLink();
		this.signInPage.sendEmptyUserNameEmptyPassword();
		this.signInPage.clickSigninActionButton();

		logger.debug("Tc_03_SignInPageEmptyUsernameEmptyPassword - end");
		boolean isTestPassed = this.signInPage.verifyEmptyUsernameEmptyPassword();
		if (isTestPassed) {
			SignInPageEmptyUsernameEmptyPassword.pass("SignInPageEmptyUsernameEmptyPassword passed");
		} else {
			SignInPageEmptyUsernameEmptyPassword.fail("SignInPageEmptyUsernameEmptyPassword fail");
		}

		assertTrue(isTestPassed);

	}

	 @Test
	public void Tc_04_SignInPageInValidEmailAddressFormat() throws InterruptedException, IOException {
		logger.debug(" Tc_04_SignInPageInValidEmailAddressFormatValidPassword - start");
		ExtentTest SignInPageInValidEmailAddressFormatValidPassword = this.getExtentReport()
				.createTest("Tc_04_SignInPageInValidEmailAddressFormatValidPassword");
		
		this.signInPage.clickSignInButton();
		this.signInPage.clickSignInnerLink();
		this.signInPage.sendInValidEmailAddressFormatValidPassword();
		this.signInPage.clickSigninActionButton();

		logger.debug("Tc_04_SignInPageInValidEmailAddressFormatValidPassword - end");
		
		boolean isTestPassed = this.signInPage.verifyInValidEmailAddressFormatValidPassword();
		if (isTestPassed) {
			SignInPageInValidEmailAddressFormatValidPassword.pass("SignInPageInValidEmailAddressFormatValidPassword");
		} else {
			SignInPageInValidEmailAddressFormatValidPassword.fail("SignInPageInValidEmailAddressFormatValidPassword");
		}
		assertTrue(isTestPassed);
	}

	 @Test
	public void Tc_05_SignInPageValidEmailAddressInvalidPassword() throws InterruptedException, IOException {
		logger.debug(" Tc_05_SignInPageValidEmailAddressInvalidPassword- start");
		ExtentTest SignInPageValidEmailAddressInvalidPassword = this.getExtentReport()
				.createTest("Tc_05_SignInPageValidEmailAddressInvalidPassword");
		
		this.signInPage.clickSignInButton();
		this.signInPage.clickSignInnerLink();
		this.signInPage.SignInPageValidEmailAddressInvalidPassword();
		this.signInPage.clickSigninActionButton();

		logger.debug("Tc_05_SignInPageValidEmailAddressInvalidPassword - end");
		
		boolean isTestPassed = this.signInPage.verifySignInPageValidEmailAddressInvalidPassword();
		if (isTestPassed) {
			SignInPageValidEmailAddressInvalidPassword.pass("SignInPageInValidEmailAddressFormatValidPassword");
		} else {
			SignInPageValidEmailAddressInvalidPassword.fail("SignInPageInValidEmailAddressFormatValidPassword");
		}
		assertTrue(isTestPassed);

	}

	@AfterMethod
	public void terminateBrowser() throws InterruptedException {
		
		this.getExtentReport().flush();
		termination();

	}

}
