package com.dollardays.common;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BasePage extends BaseTest {
	private Logger logger = LoggerFactory.getLogger(BasePage.class);

	public static final int Page_Load_Timeout = 20;

	public static final int Implicit_Wait = 10;

	public void verifyElementpresent(WebElement ele) {

		WebDriverWait w = new WebDriverWait(driver, Implicit_Wait);
		try {
			w.until(ExpectedConditions.visibilityOf(ele));
			logger.info(ele + " was found");
		}

		catch (Exception e) {
			logger.error("Unable to find the element " + ele + ": " + e);

		}

	}
}
