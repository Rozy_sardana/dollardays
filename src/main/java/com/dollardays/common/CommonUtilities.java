package com.dollardays.common;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommonUtilities {
	private static Properties prop;

//	public static String getPropertyValue(String key ) throws IOException {
//		if(prop == null) {
//			FileInputStream ip;
//			prop = new Properties();
//
//			ip = new FileInputStream("config.properties");
//			prop.load(ip);
//		}
//		return prop.getProperty(key);
//	}

	private static final String CONFIGURATION = "config.properties";
	 private static Logger logger =LoggerFactory.getLogger(CommonUtilities.class);

	public static String getPropertyValue(String key) throws IOException {
		if (prop == null) {
			prop = new Properties();
			prop.load(CommonUtilities.class.getClassLoader().getResourceAsStream(CONFIGURATION));
		}
		return prop.getProperty(key);
	}

//	public static String getPropertyValue(String key) throws IOException {
//		  if (prop == null) {
//		   prop = new Properties();
//		   prop.load(CommonUtilities.class.getClassLoader().getResourceAsStream(CONFIGURATION));
//		  }
//		  return prop.getProperty(key);
//		 }

	public static void captureScreenShot(WebDriver ldriver, String fileName, String folder) throws IOException {
		// Take screenshot and store as a file format
		File src = ((TakesScreenshot) ldriver).getScreenshotAs(OutputType.FILE);
		try {
			// now copy the screenshot to desired location using copyFile method
			FileUtils.copyFile(src, new File(".\\Results\\Screenshots\\" + folder + "\\" + fileName + ".png"));
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}

}
